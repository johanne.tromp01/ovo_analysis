# OpenVirtualObjects (OVO) 

OpenVirtualObjects (OVO): An open set of standardized and validated 3D household objects for virtual reality-based research, assessment, and therapy. 

The objects and the experiment can be found here: https://edmond.mpdl.mpg.de/imeji/collection/7L7t07UXD8asG_MI

![Example objects](/figures/Figure1.png)

The objects were rated for Recognizability, Familiarity, Details (i.e., visual complexity), Contact, Usage (i.e., frequency of usage in daily life) by older and younger adults. The participants also named and categorised the objects.

This repository contains the analyses described in: OpenVirtualObjects (OVO): An open set of standardized and validated 3D household objects for virtual reality-based research, assessment, and therapy (add link to paper)

* **Descriptive statistics** of all dimensions for older and younger adults.
* **Correlation analysis** for dimensions.
* **Name Agreement (NA)** and **H-statistic** for object names for older and younger adults.
* Distribution of objects over different **categories** for older and younger adults.
* Comparison with existing object/photo **databases**.





## Getting started

### Prerequisites

Information on how to clone a gitlab repository can be found here: https://learn.hibbittsdesign.org/gitlab-githubdesktop/cloning-a-gitlab-repo

Information on how to install R, Rstudio can be found here:
https://rstudio.com/products/rstudio/download/

### Analysis

* Please run all analyses (OVO_analysis_complete.Rmd) from within the .Rproj 'ovo_analysis'

* For a quick overview of the dimensions per object consider:
obj_output_all.csv 


## Contributing

## Authors

* Johanne Tromp, Felix Klotzsche, Michael Gaebler


